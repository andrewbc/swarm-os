#define INTERRUPTS_H

#ifndef COMMON_H
#include "common.h"
#endif

#define PUSHAQ    __asm__("push %rax"); \
                  __asm__("push %rbx"); \
                  __asm__("push %rcx"); \
                  __asm__("push %rdx"); \
                  __asm__("push %rbp"); \
                  __asm__("push %rdi"); \
                  __asm__("push %rsi"); \
                  __asm__("push %r8"); \
                  __asm__("push %r9"); \
                  __asm__("push %r10"); \
                  __asm__("push %r11"); \
                  __asm__("push %r12"); \
                  __asm__("push %r13"); \
                  __asm__("push %r14"); \
                  __asm__("push %r15");

#define POPAQ     __asm__("pop %r15"); \
                  __asm__("pop %r14"); \
                  __asm__("pop %r13"); \
                  __asm__("pop %r12"); \
                  __asm__("pop %r11"); \
                  __asm__("pop %r10"); \
                  __asm__("pop %r9"); \
                  __asm__("pop %r8"); \
                  __asm__("pop %rsi"); \
                  __asm__("pop %rdi"); \
                  __asm__("pop %rbp"); \
                  __asm__("pop %rdx"); \
                  __asm__("pop %rcx"); \
                  __asm__("pop %rbx"); \
                  __asm__("pop %rax");

#define ISR_BEGIN __asm__("cli");                         \
                  PUSHAQ                       \
                  __asm__("mov %ax, %ds");                  \
                  __asm__("push %eax");                    \
                  __asm__("mov %ax, 0x10");                \
                  __asm__("mov %ds, %ax");                  \
                  __asm__("mov %es, %ax");                  \
                  __asm__("mov %fs, %ax");                  \
                  __asm__("mov %gs, %ax");

#define ISR_END   __asm__("pop %eax");                     \
                  __asm__("mov %ds, %ax");                  \
                  __asm__("mov %es, %ax");                  \
                  __asm__("mov %fs, %ax");                  \
                  __asm__("mov %gs, %ax");                  \
                  POPAQ                        \
                  __asm__("add %esp, 8");                  \
                  __asm__("sti");                         \
                  __asm__("iret");

#define IRQ_BEGIN __asm__("cli");                         \
                  PUSHAQ                       \
                  __asm__("mov %ax, %ds");                  \
                  __asm__("push %eax");                    \
                  __asm__("mov %ax, 0x10");                \
                  __asm__("mov %ds, %ax");                  \
                  __asm__("mov %es, %ax");                  \
                  __asm__("mov %fs, %ax");                  \
                  __asm__("mov %gs, %ax");

#define IRQ_END   __asm__("pop %ebx");                     \
                  __asm__("mov %ds, %bx");                  \
                  __asm__("mov %es, %bx");                  \
                  __asm__("mov %fs, %bx");                  \
                  __asm__("mov %gs, %bx");                  \
                  POPAQ                        \
                  __asm__("add %esp, 8");                  \
                  __asm__("sti");                         \
                  __asm__("iret");

typedef struct registers {
    uint32  ds; // data segment selector
    uint32  edi, esi, ebp, esp, ebx, edx, ecx, eax; // pushed by pusha
    uint32  int_num, err_code; // interrupt number and error code (if applicable)
    uint32  eip, cs, eflags, useresp, ss; // pushed by processor automatically
} registers_t;

typedef void (*isr_t)(registers_t);
void register_interrupt_handler(uint8 n, isr_t handler);