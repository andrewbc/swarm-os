#define MEM_H

#ifndef COMMON_H
#include "common.h"
#endif

void kmeminit(void);
uintptr kmalloc(uinty size, bool align, uintptr *phys);
uintptr kmalloc_a(uinty size);
uintptr kmalloc_p(uinty size, uintptr *phys);
uintptr kmalloc_ap(uinty size, uintptr *phys);
