#define COMMON_H

/*
 * basic types
 */
typedef signed char             int8;
typedef unsigned char           uint8;
typedef signed short            int16;
typedef unsigned short          uint16;
typedef signed int              int32;
typedef unsigned int            uint32;
typedef signed long long int    int64;
typedef unsigned long long int  uint64;
typedef float                   float32;
typedef double                  float64;

#ifdef _64BIT
typedef uint64          uintptr;
typedef int64           intptr;
typedef uint64          uinty;
typedef int64           inty;
#else
typedef uint32          uintptr;
typedef int32           intptr;
typedef uint32          uinty;
typedef int32           inty;
#endif

typedef uint8                   bool;
typedef uint8                   byte;

enum {
  true = 1,
  false = 0,
};

struct String {
  byte *str;
  uint32 len;
};


void reset(void);
void hang(void);

#define nelem(x)        (sizeof(x)/sizeof((x)[0]))
#define nil             ((void*)0)

// MONITOR STUFF
/* The number of columns. */
#define COLUMNS 80
/* The number of lines. */
#define LINES 24

void monitor_put(byte c);
void monitor_clear();
void monitor_write(byte *c);
static void monitor_apply_cursor();
static void monitor_scroll();


#define PANIC(msg) panic((byte*)msg, (byte*)__FILE__, (uinty)__LINE__);
#define ASSERT(b) ((b) ? (void)0 : panic_assert((byte*)__FILE__, (uinty)__LINE__, #b))
#define VIRT2REAL(a) a
#define REAL2VIRT(a) a

extern void panic(byte *message, byte *file, uinty line);
extern void panic_assert(byte *file, uinty line, byte * desc);

void bus_write(uint16 port, uint8 value);
uint8 bus_read(uint16 port);
uint16 bus_readw(uint16 port);

void itoa(byte *, int32, int32);
void printf(const byte *format, ...);
void *memset(void *s, int32 c, uint32 n);
//static inline void cpuid(int32 code);

/*
 * get rid of C types
 * the / / / forces a syntax error immediately,
 * which will show "last name: XXunsigned".
 */
#define unsigned                XXunsigned / / /
#define signed                  XXsigned / / /
#define char                    XXchar / / /
#define short                   XXshort / / /
#define int                     XXint / / /
#define long                    XXlong / / /
#define float                   XXfloat / / /
#define double                  XXdouble / / /

