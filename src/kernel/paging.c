#ifndef PAGING_H
#include "paging.h"
#endif

#ifndef MEM_H
#include "mem.h"
#endif

page_directory_t *kernel_directory = 0;
page_directory_t *current_directory = 0;

uintptr *frames;
uinty   nframes;

extern uintptr placement_address; // defined in mem.c

#define INDEX_FROM_BIT(a) (a/(8*4))
#define OFFSET_FROM_BIT(a) (a%(8*4))

// frames related below this point
static void set_frame(uintptr frame_addr) {
    uintptr frame = frame_addr / 0x1000;
    uinty i = INDEX_FROM_BIT(frame);
    uinty o = OFFSET_FROM_BIT(frame);
    frames[i] |= (0x1 << o);
}

static void clear_frame(uintptr frame_addr) {
    uintptr frame = frame_addr / 0x1000;
    uinty i = INDEX_FROM_BIT(frame);
    uinty o = OFFSET_FROM_BIT(frame);
    frames[i] &= ~(0x1 << o);
}

static bool test_frame(uintptr frame_addr) {
    uintptr frame = frame_addr / 0x1000;
    uinty i = INDEX_FROM_BIT(frame);
    uinty o = OFFSET_FROM_BIT(frame);
    return (bool)(frames[i] & (0x1 << o));
}

static uintptr get_free_frame() {
    uinty i, j;
    for (i=0; i<INDEX_FROM_BIT(nframes); ++i) {
        if (frames[i] != 0xFFFFFFFF) { // nothing free, exit early.
            for (j=0; j<32; ++j) {
                uinty to_test = 0x1 << j;
                if (!(frames[i]&to_test)) {
                    return i*4*8+j;
                }
            }
        }
    }
    // if we get here, we're fucked. Pass max back;
    return (uintptr)-1;
}

void frame_alloc(page_t *page, bool for_kernel, bool is_writeable) {
    if (page->frame != 0) {
        return; // frame already allocated
    } else {
        uintptr idx = get_free_frame();
        if (idx == (uintptr)-1) {
            PANIC((byte*)"No free frames!");
        }
        set_frame(idx*0x1000); // mark it ours
        page->is_present = 1;
        page->is_writeable = (is_writeable > 0);
        page->for_user = (!(for_kernel > 0));
        page->frame = idx;
    }
}

void frame_free(page_t *page) {
    uintptr frame;
    if (!(frame = page->frame)) {
        return; // page didn't have allocated frame...
    } else {
        clear_frame(frame);
        page->frame = 0x0;
    }
}


// paging related below this point

/*
    initialise_paging firstly creates the frames bitset, and sets everything to
    zero using memset. Then it allocates space (which is page-aligned) for a
    page directory. After that, it allocates frames such that any page access
    will map to the frame with the same linear address, called identity-mapping.
    This is done for a small section of the address space, so the kernel code
    can continue to run as normal. It registers an interrupt handler for page
    faults (below) then enables paging.
*/
void initialize_paging() {
    printf((byte*)"Initializing paging.");
    uint32 mem_end_page = 0x1000000;
    nframes = mem_end_page / 0x1000;
    frames = (uintptr*)kmalloc_a(INDEX_FROM_BIT(nframes));
    memset(frames, 0, INDEX_FROM_BIT(nframes));

    // make page directory
    kernel_directory = (page_directory_t *) kmalloc_a(sizeof(page_directory_t));
    memset(kernel_directory, 0, sizeof(page_directory_t));
    current_directory = kernel_directory;

    // We need to identity map (phys addr = virt addr) from
    // 0x0 to the end of used memory, so we can access this
    // transparently, as if paging wasn't enabled.
    // NOTE that we use a while loop here deliberately.
    // inside the loop body we actually change placement_address
    // by calling kmalloc(). A while loop causes this to be
    // computed on-the-fly rather than once at the start.
    uinty i = 0;
    while (i < placement_address) {
        // Kernel code is readable but not writeable from userspace.
        frame_alloc( get_page(i, 1, kernel_directory), 0, 0);
        i += 0x1000;
    }
    // Before we enable paging, we must register our page fault handler.
    register_interrupt_handler(14, page_fault);

    // Now, enable paging!
    load_page_directory(kernel_directory);
}

void load_page_directory(page_directory_t *dir) {
    current_directory = dir;
    __asm__ volatile("mov %0, %%cr3":: "r"(&dir->physicals_addr));
    uint32 cr0;
    __asm__ volatile("mov %%cr0, %0": "=r"(cr0));
    cr0 |= 0x8000000; // enable paging
    __asm__ volatile("mov %0, %%cr0":: "r"(cr0));
}

page_t *get_page(uintptr address, bool make, page_directory_t *dir) {
    // turn address into an index.
    address /= 0x1000;
    uint32 table_idx = address / 1024;
    if (dir->tables[table_idx]) { // already assigned
        return &dir->tables[table_idx]->pages[address%1024];
    } else if (make) {
        uint32 tmp;
        dir->tables[table_idx] = (page_table_t*)kmalloc_ap(sizeof(page_table_t), &tmp);
        memset(dir->tables[table_idx], 0, 0x1000);
        dir->tables_physical[table_idx] = tmp | 0x7; // is_present, is_writeable, for_user
        return &dir->tables[table_idx]->pages[address%1024];
    } else {
        return 0;
    }
}

void page_fault(registers_t regs) {
    // A page fault has occurred.
    // The faulting address is stored in the CR2 register.
    uintptr faulting_address;
    __asm__ volatile("mov %%cr2, %0" : "=r" (faulting_address));

    // The error code gives us details of what happened.
    bool is_present   = !(regs.err_code & 0x1); // Page not present
    bool is_writeable = regs.err_code & 0x2;    // Write operation?
    bool for_user = regs.err_code & 0x4;        // Processor was in user-mode?
    bool reserved = regs.err_code & 0x8;        // Overwritten CPU-reserved bits of page entry?
    bool id = regs.err_code & 0x10;             // Caused by an instruction fetch?

    // Output an error message.
    printf((byte*)"Page fault! (%s, %s, %s, %s, %s) at 0x%x\n",
           (is_present) ? "is_present ": "!is_present",
           (is_writeable) ? "is_writeable ": "!is_writeable",
           (for_user) ? "for_user ": "!for_user",
           (reserved) ? "reserved ": "!reserved",
           (id) ? "instr_fetch": "!instr_fetch",
           faulting_address);
    PANIC("Page fault");
}
