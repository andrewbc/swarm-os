#ifndef TIMER_H
#include "timer.h"
#endif

#ifndef INTERRUPTS_H
#include "interrupts.h"
#endif

uint32 tick = 0;

static void timer_callback(registers_t regs) {
    ++tick;
    printf((byte *)"Tick: %d\n", tick);
}

void init_timer(uint32 frequency) {
    register_interrupt_handler(32, &timer_callback);
    
    // The value we send to the PIT is the value to divide it's input clock
    // (1193180 Hz) by, to get our required frequency. Important to note is
    // that the divisor must be small enough to fit into 16-bits.
    uint32 divisor = 1193180 / frequency;
    
    bus_write(0x43, 0x36); // send command byte
    uint8 l = (uint8)(divisor & 0xFF);
    uint8 h = (uint8)((divisor>>8) & 0xFF);
    bus_write(0x40, l); // send frequency divisor
    bus_write(0x40, h);
}
