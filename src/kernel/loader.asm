global Entry           ; making entry point visible to linker
extern kmain            ; kmain is defined elsewhere
 
; setting up the Multiboot header - see GRUB docs for details
MODULEALIGN equ  1<<0 ; align loaded modules on 4KB page boundaries
MEMINFO     equ  1<<1 ; provide memory map
VMT_INFO    equ  1<<2 ; provide video mode table info

FLAGS       equ MODULEALIGN | MEMINFO   ; this is the Multiboot 'flag' field
MAGIC       equ 0x1BADB002              ; 'magic number' lets bootloader find the header
CHECKSUM    equ -(MAGIC + FLAGS)        ; checksum required

; This is the virtual base address of kernel space. It must be used to convert virtual
; addresses into physical addresses until paging is enabled. Note that this is not
; the virtual address where the kernel image itself is loaded -- just the amount that must
; be subtracted from a virtual address to get a physical address.
KERNEL_VIRTUAL_BASE equ 0xC0000000                  ; Who needs a higher half kernel anyway?
KERNEL_PAGE_NUMBER equ (KERNEL_VIRTUAL_BASE >> 22)  ; Page directory index of kernel's 4MB PTE.

;section .data align = 8
;PageDir:
    ; This page directory entry identity-maps the first 4MB of the 32-bit physical address space.
    ; All bits are clear except the following:
    ; bit 7: PS The kernel page is 4MB.
    ; bit 1: RW The kernel page is read/write.
    ; bit 0: P  The kernel page is present.
    ; This entry must be here -- otherwise the kernel will crash immediately after paging is
    ; enabled because it can't fetch the next instruction! It's ok to unmap this page later.
;    dd 0x00000083
;    times (KERNEL_PAGE_NUMBER-1) dd 0         ; Pages before kernel space.

    ; This page directory entry defines a 4MB page containing the kernel.
;    dd 0x00000083
;    times (1024 - KERNEL_PAGE_NUMBER - 1) dd 0  ; Pages after the kernel image.


section .text align = 4
MultiBootHeader:
   dd MAGIC
   dd FLAGS
   dd CHECKSUM

; reserve initial kernel stack space -- that's 16k.
STACKSIZE equ 0x4000

Entry:
    mov rsp, Stack+STACKSIZE   ; set up the stack

    push rbx                   ; pass Multiboot info structure
    push rax                   ; pass Multiboot magic number

    call kmain                 ; call kernel proper
    jmp Hang

Hang:
   hlt                         ; halt machine should kernel return
   jmp Hang

section .bss align = 32
Stack:
   resb STACKSIZE              ; reserve 16k stack on a quadword boundary
