// interrupts.c -- High level interrupt service routines and interrupt request handlers.

#ifndef INTERRUPTS_H
#include "interrupts.h"
#endif

isr_t interrupt_handlers[256];

void register_interrupt_handler(uint8 n, isr_t handler) {
   interrupt_handlers[n] = handler;
}

/*
void irq_handler(registers_t regs) {
   // Send an EOI (end of interrupt) signal to the PICs.
   // If this interrupt involved the slave.
   if (regs.int_num >= 40) {
       // Send reset signal to slave.
       bus_write(0xA0, 0x20);
   }
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   if (interrupt_handlers[regs.int_num] != 0) {
       isr_t handler = interrupt_handlers[regs.int_num];
       handler(regs);
   }
}
*/

void __attribute__((naked)) isr_0(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 0);
   ISR_END
}

void __attribute__((naked)) isr_1(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 1);
   ISR_END   
}

void __attribute__((naked)) isr_2(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 2);
   ISR_END   
}

void __attribute__((naked)) isr_3(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 3);
   ISR_END   
}

void __attribute__((naked)) isr_4(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 4);
   ISR_END   
}

void __attribute__((naked)) isr_5(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 5);
   ISR_END   
}

void __attribute__((naked)) isr_6(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 6);
   ISR_END   
}

void __attribute__((naked)) isr_7(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 7);
   ISR_END   
}

void __attribute__((naked)) isr_8(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 8);
   ISR_END   
}

void __attribute__((naked)) isr_9(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 9);
   ISR_END   
}

void __attribute__((naked)) isr_10(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 10);
   ISR_END   
}

void __attribute__((naked)) isr_11(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 11);
   ISR_END   
}

void __attribute__((naked)) isr_12(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 12);
   ISR_END   
}

void __attribute__((naked)) isr_13(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 13);
   ISR_END   
}

void __attribute__((naked)) isr_14(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 14);
   ISR_END   
}

void __attribute__((naked)) isr_15(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 15);
   ISR_END   
}

void __attribute__((naked)) isr_16(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 16);
   ISR_END   
}

void __attribute__((naked)) isr_17(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 17);
   ISR_END   
}

void __attribute__((naked)) isr_18(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 18);
   ISR_END   
}

void __attribute__((naked)) isr_19(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 19);
   ISR_END   
}

void __attribute__((naked)) isr_20(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 20);
   ISR_END   
}

void __attribute__((naked)) isr_21(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 21);
   ISR_END   
}

void __attribute__((naked)) isr_22(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 22);
   ISR_END   
}

void __attribute__((naked)) isr_23(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 23);
   ISR_END   
}

void __attribute__((naked)) isr_24(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 24);
   ISR_END   
}

void __attribute__((naked)) isr_25(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 25);
   ISR_END   
}

void __attribute__((naked)) isr_26(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 26);
   ISR_END   
}

void __attribute__((naked)) isr_27(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 27);
   ISR_END   
}

void __attribute__((naked)) isr_28(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 28);
   ISR_END   
}

void __attribute__((naked)) isr_29(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 29);
   ISR_END   
}

void __attribute__((naked)) isr_30(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 30);
   ISR_END   
}

void __attribute__((naked)) isr_31(void) {
   ISR_BEGIN
   printf((byte *)"received interrupt: %d\n", 31);
   ISR_END   
}


/* IRQs */
void __attribute__((naked)) irq_0(void) {
   IRQ_BEGIN
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END
}

void __attribute__((naked)) irq_1(void) {
   IRQ_BEGIN
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}

void __attribute__((naked)) irq_2(void) {
   IRQ_BEGIN
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}

void __attribute__((naked)) irq_3(void) {
   IRQ_BEGIN
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}

void __attribute__((naked)) irq_4(void) {
   IRQ_BEGIN
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}

void __attribute__((naked)) irq_5(void) {
   IRQ_BEGIN
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}

void __attribute__((naked)) irq_6(void) {
   IRQ_BEGIN
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}

void __attribute__((naked)) irq_7(void) {
   IRQ_BEGIN
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}

// involves slaves, below
void __attribute__((naked)) irq_8(void) {
   IRQ_BEGIN

   // Send an EOI (end of interrupt) signal to the PICs.
   // Since this interrupt involved the slave.
   bus_write(0xA0, 0x20);
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}

void __attribute__((naked)) irq_9(void) {
   IRQ_BEGIN
   // Send an EOI (end of interrupt) signal to the PICs.
   // Since this interrupt involved the slave.
   bus_write(0xA0, 0x20);
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}

void __attribute__((naked)) irq_10(void) {
   IRQ_BEGIN
   // Send an EOI (end of interrupt) signal to the PICs.
   // Since this interrupt involved the slave.
   bus_write(0xA0, 0x20);
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}

void __attribute__((naked)) irq_11(void) {
   IRQ_BEGIN
   // Send an EOI (end of interrupt) signal to the PICs.
   // Since this interrupt involved the slave.
   bus_write(0xA0, 0x20);
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}

void __attribute__((naked)) irq_12(void) {
   IRQ_BEGIN
   // Send an EOI (end of interrupt) signal to the PICs.
   // Since this interrupt involved the slave.
   bus_write(0xA0, 0x20);
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}

void __attribute__((naked)) irq_13(void) {
   IRQ_BEGIN
   // Send an EOI (end of interrupt) signal to the PICs.
   // Since this interrupt involved the slave.
   bus_write(0xA0, 0x20);
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}

void __attribute__((naked)) irq_14(void) {
   IRQ_BEGIN
   // Send an EOI (end of interrupt) signal to the PICs.
   // Since this interrupt involved the slave.
   bus_write(0xA0, 0x20);
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}

void __attribute__((naked)) irq_15(void) {
   IRQ_BEGIN
   // Send an EOI (end of interrupt) signal to the PICs.
   // Since this interrupt involved the slave.
   bus_write(0xA0, 0x20);
   // Send reset signal to master. (As well as slave, if necessary).
   bus_write(0x20, 0x20);

   IRQ_END   
}