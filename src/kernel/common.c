#ifndef COMMON_H
#include "common.h"
#endif

void bus_write(uint16 port, uint8 value) {
    __asm__ volatile ("outb %1, %0" : : "dN" (port), "a" (value));
}

uint8 bus_read(uint16 port) {
    uint8 ret;
    __asm__ volatile ("inb %1, %0" : "=a" (ret) : "dN" (port));
    return ret;
}

uint16 bus_readw(uint16 port) {
    uint16 ret;
    __asm__ volatile ("inw %1, %0" : "=a" (ret) : "dN" (port));
    return ret;
}

extern void panic(byte *message, byte *file, uinty line) {
    __asm__ volatile("cli");
    printf((byte*)"KERNEL PANIC(%s) at %s: %d\n", message, file, line);
    for(;;);
}

extern void panic_assert(byte *file, uinty line, byte *desc) {
    __asm__ volatile("cli");
    printf((byte*)"ASSERTION FAILED(%s) at %s: %d\n", desc, file, line);
    for(;;);
}

void *memset(void *s, int32 c, uint32 n) {
    byte *p = s;
    while(n--) {
        *p++ = (byte)c;
    }
    return s;
}

void reset(void) {
    uint64 null_idtr = 0;
    __asm__("lidt %0; int3;" :: "m" (null_idtr));
}

void hang(void) {
    for(;;) {
        __asm__("hlt");
    }
}

// MONITOR STUFF
uint16 *video_memory = (uint16 *) 0x010B8000;
uint8 cursor_x = 0;
uint8 cursor_y = 0;

static void monitor_apply_cursor() {
    uint16 pos = cursor_y * COLUMNS + cursor_x;
    bus_write(0x3D4, 14);       // Setting high byte for pos
    bus_write(0x3D5, pos >> 8); // Send high byte
    bus_write(0x3D4, 15);       // Setting low byte
    bus_write(0x3D5, pos);      // Send low byte
}

static void monitor_scroll() {
    uint8 attr = (0 << 4) | (15 & 0x0F);
    uint16 blank = 0x20 | (attr << 8);
    
    
    if (cursor_y >= LINES+1) {
        uint16 i;
        for (i = 0; i < LINES*COLUMNS; ++i) {
            video_memory[i] = video_memory[i+COLUMNS];
        }
        
        for (i = LINES*COLUMNS; i < (LINES+1)*COLUMNS; ++i) {
            video_memory[i] = blank;
        }
        
        cursor_y = LINES;
        monitor_apply_cursor();
    }
}

void monitor_put(byte c) {
    uint8 bg = 0;
    uint8 fg = 15;
    
    uint8 attr = (bg << 4) | (fg & 0x0F);
    uint16 attribute = attr << 8;
    uint16 *loc;
    
    if (c == 0x08 && (byte)cursor_x) { // backspace
        --cursor_x;
    } else if (c == 0x09) { // tab
        cursor_x = (cursor_x+8) & ~(8-1);
    } else if (c == '\r') {
        cursor_x = 0;
    } else if (c == '\n') {
        cursor_x = 0;
        ++cursor_y;
    } else if (c >= ' ') { // handle printables
        loc = video_memory + (cursor_y*COLUMNS+cursor_x);
        *loc = c | attribute;
        ++cursor_x;
    }
    
    if (cursor_x >= COLUMNS) {
        cursor_x = 0;
        ++cursor_y;
    }
    
    monitor_scroll();
    monitor_apply_cursor();
}

void monitor_clear() {
    uint8 attr = (0 << 4) | (15 & 0x0F);
    uint16 blank = 0x20 | (attr << 8);
    
    uint16 i;
    for (i = 0; i < COLUMNS*(LINES+1); ++i) {
        video_memory[i] = blank;
    }
    
    cursor_x = 0;
    cursor_y = 0;
    monitor_apply_cursor();
}

void monitor_write(byte *c) {
    uint64 i = 0;
    while(c[i]) {
        monitor_put(c[i++]);
    }
}

/* Convert the integer D to a string and save the string in BUF. If
   BASE is equal to 'd', interpret that D is decimal, and if BASE is
   equal to 'x', interpret that D is hexadecimal. */
void itoa(byte *buf, int32 base, int32 d) {
  byte *p = buf;
  byte *p1, *p2;
  uint32 ud = d;
  int32 divisor = 10;

  /* If %d is specified and D is minus, put `-' in the head. */
  if (base == 'd' && d < 0)
    {
      *p++ = '-';
      buf++;
      ud = -d;
    }
  else if (base == 'x')
    divisor = 16;

  /* Divide UD by DIVISOR until UD == 0. */
  do
    {
      int32 remainder = ud % divisor;

      *p++ = (remainder < 10) ? remainder + '0' : remainder + 'a' - 10;
    }
  while (ud /= divisor);

  /* Terminate BUF. */
  *p = 0;

  /* Reverse BUF. */
  p1 = buf;
  p2 = p - 1;
  while (p1 < p2)
    {
      byte tmp = *p1;
      *p1 = *p2;
      *p2 = tmp;
      p1++;
      p2--;
    }
}

// STRING STUFF
void printf(const byte *format, ...) {
    byte **arg = (byte **) &format;
    byte c;
    byte buf[20];
    arg++;
    
    while ((c = *format++) != 0) {
        if (c != '%') {
            monitor_put(c);
        } else {
            byte *p;

            c = *format++;
            switch (c) {
                case 'd':
                case 'u':
                case 'x':
                    itoa(buf, c, *((intptr *) arg++));
                    p = buf;
                    goto string;
                break;

                case 's':
                    p = *arg++;
                    if (!p) {
                        p = (byte *)"(null)";
                    }

                string:
                    while (*p) {
                        monitor_put(*p++);
                    }
                break;

                default:
                    monitor_put(*((uintptr *) arg++));
                break;
            }
        }
    }
}
