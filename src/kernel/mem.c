#ifndef MEM_H
#include "mem.h"
#endif

extern uintptr kend; // defined in the linker script
uintptr placement_address;

void kmeminit(void) {
    placement_address = (uintptr) &kend;
}

uintptr kmalloc(uinty size, bool align, uintptr *phys) {
    
    if (align && (placement_address & 0xFFFFF000)) { // if not already page aligned
        placement_address &= 0xFFFFF000;
        placement_address += 0x1000;
    }
    
    if (phys) {
        *phys = placement_address;
    }
    
    uintptr tmp = placement_address;
    placement_address += size;
    printf((byte*)"kmalloc: loc=0x%x size=%d align=%d phys=0x%x\n", tmp, size, align, phys);
    return tmp;
}

uintptr kmalloc_a(uinty size) {
    return kmalloc(size, 1, 0);
}

uintptr kmalloc_p(uinty size, uintptr *phys) {
    return kmalloc(size, 0, phys);
}

uintptr kmalloc_ap(uinty size, uintptr *phys) {
    return kmalloc(size, 1, phys);
}