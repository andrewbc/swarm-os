#ifndef MULTIBOOT_H
#include "../multiboot.h"
#endif

#ifndef DESCRIPTOR_TABLES_H
#include "descriptor_tables.h"
#endif

#ifndef TIMER_H
#include "timer.h"
#endif

#ifndef PAGING_H
#include "paging.h"
#endif

#ifndef MEM_H
#include "mem.h"
#endif

/* The attribute of an character. */
#define ATTRIBUTE 7

/* Check if the bit BIT in FLAGS is set. */
#define CHECK_FLAG(flags,bit)   ((flags) & (1 << (bit)))

void kmain(uintptr magic, void *mbd) {
    if (magic != 0x2BADB002) {
        /* Something went not according to specs. Print an error */
        /* message and halt, but do *not* rely on the multiboot */
        /* data structure. */
        printf((byte*)"Bad magic: 0x%x\n", (uintptr)magic);
        __asm__ volatile("hlt");
    }

    kmeminit();
    init_descriptor_tables(); // Initialize all the ISRs and segmentation
    monitor_clear();
    initialize_paging();
    monitor_write((byte *)"Hello, world!");

    uintptr *ptr = (uintptr *)0xA0000000;
    printf((byte*)"herp derp %s", *ptr);
    return;
}
