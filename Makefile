ROOT_DIR = $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
LOG_FILE = build/logs/kernel_logs.txt
GRUB_STAGE_1 = src/boot/grub/stage1
GRUB_STAGE_2 = src/boot/grub/stage2
GRUB_PAD = src/boot/pad

ASM = nasm
AFLAGS = -f elf64
LD = ld
LFLAGS = -T src/kernel/linker.ld	\
	-b elf64-x86-64					

CC = clang
CFLAGS = -m64						\
	-mcmodel=medium					\
	-fPIC							\
	-integrated-as					\
	-ccc-host-triple x86_64-pc-elf	\
	-nostdlib						\
	-std=c99						\
	-W								\
	-Wall							\
	-Wextra							\
	-Werror							\
	-ffreestanding					\
	-fno-builtin					\
	-Wno-unused-function			\
	-Wno-unused-parameter			\
	-mno-red-zone					\
	-mno-mmx						\
	-mno-sse						\
	-mno-sse2						\
	-mno-sse3						\
	-mno-3dnow

COBJECTS = $(patsubst src/kernel/%.c,build/kernel/%.o,$(wildcard src/kernel/*.c))
AOBJECTS = $(patsubst src/kernel/%.asm,build/kernel/%_asm.o,$(wildcard src/kernel/*.asm))

all: kernel

build/kernel/%_asm.o: src/kernel/%.asm
	$(ASM) $(AFLAGS) -o $@ $<

build/kernel/%.o: src/kernel/%.c
	$(CC) $(CFLAGS) -o $@ -c $<

kernel: $(AOBJECTS) $(COBJECTS)
	$(LD) $(LFLAGS) $(AOBJECTS) $(COBJECTS) -o build/$@.bin
	cat $(GRUB_STAGE_1) $(GRUB_STAGE_2) $(GRUB_PAD) build/kernel.bin > build/floppy.img
	objdump -dxtwl build/kernel.bin > build/kernel.disasm
	objdump -swl -j .rodata build/kernel.bin >> build/kernel.disasm
	du -A -B 512 build/kernel.bin

clean:
	rm -rf build
	mkdir build && cd build && mkdir kernel && mkdir logs
	
test:
	bochs -f bochsrc.txt
